<?php

use App\Entity\Curso;
use App\Helper\EntityManagerFactory;

require_once __DIR__.'/../vendor/autoload.php';

$curso = new Curso;
$curso->setNome($argv[1]);

$entityManagerFactory = new EntityManagerFactory;
$entityManager = $entityManagerFactory->getEntityManager();

$entityManager->persist($curso);
$entityManager->flush();