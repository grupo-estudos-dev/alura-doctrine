<?php

use App\Entity\Curso;
use App\Helper\EntityManagerFactory;

require_once __DIR__.'/../vendor/autoload.php';

$entityManagerFactory = new EntityManagerFactory;

$entityManager = $entityManagerFactory->getEntityManager();

$cursosRepository = $entityManager->getRepository(Curso::class);
$cursos = $cursosRepository->findAll();

foreach ($cursos as $key => $curso) {
    echo $curso->getNome();
}