<?php

use App\Entity\Aluno;
use App\Entity\Telefone;
use App\Helper\EntityManagerFactory;

require_once __DIR__.'/../vendor/autoload.php';

$entityManagerFactory = new EntityManagerFactory;

$entityManager = $entityManagerFactory->getEntityManager();

$alunoRepository = $entityManager->getRepository(Aluno::class);
$alunos = $alunoRepository->findAll();

foreach ($alunos as $key => $aluno) {
    $telefones = $aluno->getTelefones()->map(function(Telefone $telefone) {
        return $telefone->getNumero();
    })
    ->toArray();
    echo "ID: {$aluno->getId()} \n Nome: {$aluno->getNome()}\n";
    echo "Telefone: " . implode(',', $telefones);
    echo "\n\n";
}