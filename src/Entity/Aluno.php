<?php

namespace App\Entity;

use App\Entity\Telefone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @Entity Aluno
 */
class Aluno
{
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * @Column(type="string")
     */
    private $nome;

    /**
     * @OneToMany(targetEntity="Telefone", mappedBy="aluno", cascade={"remove", "persist"})
     */
    private $telefones;

    /**
     * @ManyToMany(targetEntity="Curso", mappedBy="alunos")
     */
    private $cursos;

    public function __construct()
    {
        $this->telefones = new ArrayCollection;
        $this->cursos = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNome() : string
    {
        return $this->nome;
    }

    /**
     * @return self
     */
    public function setNome($nome) : self
    {
        $this->nome = $nome;
        return $this;
    }

    public function addTelefone(Telefone $telefone)
    {
        $this->telefones->add($telefone);
        $telefone->setAluno($this);
        return $this;
    }

    public function getTelefones() : Collection
    {
        return $this->telefones;
    }

    public function addCurso(Curso $curso) : self
    {
        if($this->cursos->contains($curso)) return $this;
        $this->cursos->add($curso);
        $curso->addAluno($this);

        return $this;
    }

    public function getCursos() : Collection
    {
        return $this->cursos;
    }
}